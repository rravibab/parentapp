import { registerApplication, start } from "single-spa";
import {getCount} from  './shell/App'

// registerApplication({
//   name: "infy/root-config",
//   app: () => System.import("infy/root-config"),
//   activeWhen: ["/"],
// });
registerApplication(
  'app2',
  () => import('./shell/shell.app.js'),
  (location) => location.pathname.startsWith('/'),
  { some: 'value' }
);
registerApplication(
  "infy-Mf",
  () => System.import("infy-Mf"),
  (location) => location.pathname.startsWith('/microApp1'),
  { data:getCount
   }, 
);
registerApplication(
  "infy-Mf2",
  () => System.import("infy-Mf2"),
  (location) => location.pathname.startsWith('/microApp2'),
  { data:getCount
   }, 
);
registerApplication(
  "my-app",
  () => System.import("my-app"),
  (location) => location.pathname.startsWith('/angularApp'), 
);
// registerApplication({
//   name: "infy-Mf",
//   app: () => System.import("infy-Mf"),
//   activeWhen: ["/micro"]
// });


start({
  urlRerouteOnly: true,
});
