
import React from "react";
import { Shell, Panel } from "@informatica/droplets-core";
import {getCount} from "./App.js"

const HomeApp = (props) => {  
    let count=getCount();
    return (<div>
        <Shell.Page>
    <Panel title="Parent React Application ">
    This is Home page of Parent App 
    </Panel>
    <Panel title="Count to be updated in Child  Application ">
      Current Count : {count}    </Panel>
    </Shell.Page>
        </div>);
};

export const Home = {
    path: "/",
    component: HomeApp,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Home'
        }
    }
};
