import React from "react";
import { Shell } from '@informatica/droplets-common';
import { routes } from './Routes';
 var count=0;
window.addEventListener("myEvent", myEventHandlingFunction)

export const App = () => {
  return <Shell style={{ width: "100%" }} productName="ParentApp"
    routes={[...routes]}
    basePath="/"
    extensibility={"true"}>
    <Shell.Header productName="Parent Application"></Shell.Header>
    <Shell.Nav />
    <Shell.Main />
  </Shell>
}

function myEventHandlingFunction(e) {
  console.log("vikram111",e.detail)
  count=e.detail
  const event = new CustomEvent("updateEvent", {detail : "updated count"})
  window.dispatchEvent(event)
}
export function getCount(){
  return count;
}