
import React, { useEffect } from "react";
import { Shell } from "@informatica/droplets-core";
import { navigateToUrl } from 'single-spa';


const Micro = () => {
    useEffect(() => {
        // const script = document.createElement('script');
        // script.src = System.import("infy-Mf")
        // script.async = true;
        // document.body.appendChild(script);
        // return () => {
        //     document.body.removeChild(script);
        // }
    }, []);

    return (" ");
};

export const MicroApp = {
    path: "/microApp1",
    component: Micro,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Micro app_1'
        }
    }
};
