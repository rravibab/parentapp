import React from "react";
import { Shell } from '@informatica/droplets-common';

const InnerApp = () => {
    return (" ");
}

export const InnerAppContainer = {
    path: "/angularApp",
    component: InnerApp,
    preserveState: "children",
    meta: {
        nav: {
            label: 'Angular app'
        }
    }
};
