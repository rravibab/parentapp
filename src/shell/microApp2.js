
import React from "react";
import { Shell, Panel } from "@informatica/droplets-core";

const dummy = (props) => {  
    return (" ");
};

export const MicroApp2 = {
    path: "/microApp2",
    component: dummy,
    preserveState: "children",
    meta: {
        nav: {
            label: 'micro App_2'
        }
    }
};
